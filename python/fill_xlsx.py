import openpyxl
from openpyxl import Workbook
from openpyxl import load_workbook

#------------------- Variables Globales ------------------------ #
filename = "example.xlsx"
wb = load_workbook(filename)

def save():
    wb.save("championship.xlsx")

if __name__ == '__main__':
    print("Hello World!\n")
    ws1 = wb.create_sheet("second")
    print(wb.sheetnames)
