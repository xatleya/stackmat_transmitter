import mysql.connector

def get_website(user_id):
    conn = mysql.connector.connect(host="localhost", user="xion", password="password", database="cubecomps_info")
    cursor = conn.cursor()
    cursor.execute("SELECT website FROM users WHERE id=\"{}\"".format(user_id))
    rows = cursor.fetchall()
    for row in rows:
        result = row[0]
    conn.close()
    return result

def insert_compname(user_id, name):
    conn = mysql.connector.connect(host="localhost", user="xion", password="password", database="cubecomps_info")
    cursor = conn.cursor()
    cursor.execute("UPDATE users SET comp_name=\"{}\" WHERE id=\"{}\"".format(name, user_id))
    conn.close()

def insert_competitor(user_id, name, country):
    conn = mysql.connector.connect(host="localhost", user="xion", password="password", database="cubecomps_info")
    cursor = conn.cursor()

    cursor.execute("SELECT id FROM competitors{} WHERE name=\"{}\"".format(user_id, name))
    rows = cursor.fetchall()
    result = None
    for row in rows:
        result = row[0]

    if result == None:
        cursor.execute("INSERT INTO competitors{}(name, country) VALUES (\"{}\", \"{}\")".format(user_id, name, country))
    else:
        cursor.execute("UPDATE competitors{} SET name=\"{}\", country=\"{}\" WHERE id=\"{}\"".format(user_id, name, country, result ))
    conn.close()

def insert_events(user_id, name):
    conn = mysql.connector.connect(host="localhost", user="xion", password="password", database="cubecomps_info")
    cursor = conn.cursor()

    cursor.execute("SELECT id FROM categories WHERE name=\"{}\"".format(name))
    rows = cursor.fetchall()
    cat_id = None
    for row in rows:
        cat_id = row[0]

    cursor.execute("SELECT id FROM events{} WHERE id=\"{}\"".format(user_id, cat_id))
    rows = cursor.fetchall()
    result = None
    for row in rows:
        result = row[0]

    if result == None:
        cursor.execute("INSERT INTO events{}(id) VALUES (\"{}\")".format(user_id, cat_id))
    else:
        cursor.execute("UPDATE events{} SET id=\"{}\" WHERE id=\"{}\"".format(user_id, cat_id, result))
    conn.close()

def insert_event_info(user_id, name, rounds, format, group_size, cutoff):
    conn = mysql.connector.connect(host="localhost", user="xion", password="password", database="cubecomps_info")
    cursor = conn.cursor()

    cursor.execute("SELECT id FROM categories WHERE name=\"{}\"".format(name))
    rows = cursor.fetchall()
    cat_id = None
    for row in rows:
        cat_id = row[0]

    cursor.execute("SELECT id FROM formats WHERE name=\"{}\"".format(format))
    rows = cursor.fetchall()
    format_id = None
    for row in rows:
        format_id = row[0]

    if rounds == 1:
        cursor.execute("UPDATE events{} SET r1=\"1\", r1_format=\"{}\", r1_groupsize=\"{}\" WHERE id=\"{}\"".format(user_id, format_id, group_size, cat_id))
    elif rounds == 2:
        cursor.execute("UPDATE events{} SET r1=\"1\", r1_format=\"{}\", r1_groupsize=\"{}\", r2=\"1\", r2_format=\"{}\" WHERE id=\"{}\"".format(user_id, format_id, group_size, format_id, cat_id))
    elif rounds == 3:
        cursor.execute("UPDATE events{} SET r1=\"1\", r1_format=\"{}\", r1_groupsize=\"{}\", r2=\"1\", r2_format=\"{}\", r3=\"1\", r3_format=\"{}\" WHERE id=\"{}\"".format(user_id, format_id, group_size, format_id, format_id, cat_id))
    elif rounds == 4:
        cursor.execute("UPDATE events{} SET r1=\"1\", r1_format=\"{}\", r1_groupsize=\"{}\", r2=\"1\", r2_format=\"{}\", r3=\"1\", r3_format=\"{}\", r4=\"1\", r4_format=\"{}\" WHERE id=\"{}\"".format(user_id, format_id, group_size, format_id, format_id, format_id, cat_id))

    if cutoff != "0":
        cursor.execute("UPDATE events{} SET timelimit=\"{}\" WHERE id=\"{}\"".format(user_id, cutoff, cat_id))

    conn.close()

def insert_registration(user_id, cat_name, comp_name):
    conn = mysql.connector.connect(host="localhost", user="xion", password="password", database="cubecomps_info")
    cursor = conn.cursor()

    cursor.execute("SELECT id FROM categories WHERE name=\"{}\"".format(cat_name))
    rows = cursor.fetchall()
    cat_id = None
    for row in rows:
        cat_id = row[0]

    cursor.execute("SELECT id FROM competitors{} WHERE name=\"{}\"".format(user_id, comp_name))
    rows = cursor.fetchall()
    comp_id = None
    for row in rows:
        comp_id = row[0]

    cursor.execute("SELECT round FROM registrations{} WHERE cat_id=\"{}\" AND comp_id=\"{}\"".format(user_id, cat_id, comp_id))
    rows = cursor.fetchall()
    result = None
    for row in rows:
        result = row[0]


    if result == None:
        cursor.execute("INSERT INTO registrations{}(cat_id, round, comp_id) VALUES (\"{}\", \"1\", \"{}\")".format(user_id, cat_id, comp_id))
    conn.close()


if __name__ == '__main__':
   print(get_website("1"))
   insert_compname("1", "test")
   insert_competitor("1", "Aaron Roberts", "France")