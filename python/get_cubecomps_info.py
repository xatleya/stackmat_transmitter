import urllib.request as rq
from bs4 import BeautifulSoup
import sys
import talk_to_database


events_list = ["Rubik's Cube",
               "2x2x2 Cube",
               "4x4x4 Cube", "5x5x5 Cube",
               "6x6x6 Cube", "7x7x7 Cube",
               "3x3x3 Blindfolded",
               "3x3x3 Fewest Moves",
               "3x3x3 One-Handed",
               "3x3x3 With Feet",
               "Megaminx",
               "Pyraminx",
               "Rubik's Clock",
               "Skewb",
               "Square-1",
               "4x4x4 Blindfolded",
               "5x5x5 Blindfolded",
               "3x3x3 Multi-Blind"]


# information for the registration page of the xlsx
class Registration:
    def __init__(self, title):
        self.title = title
        self.competitors = []

    def print(self):
        str = self.title
        str += "\n"
        print(str)


# information needed for an event
class Event:
    def __init__(self, type, rounds, format):
        self.type = type
        self.rounds = rounds
        self.format = format
        self.competitors = []
        self.cutoff = None


class Competitors:
    def __init__(self, name, country):
        self.name = name
        self.country = country

    def __eq__(self, other):
        if self.name == other:
            return True
        else:
            return False

    def __ne__(self, other):
        if self.name != other:
            return True
        else:
            return False

    def print(self):
        print("name : {}, country : {}".format(self.name, self.country))


def find_first_between(string, start, end):
    part1 = (string.split(start))[1]
    part2 = (part1.split(end))[0]
    return part2


def find_all_between(string, start, end, count):
    result = []
    tab = (string.split(start))
    for i in range(0,count):
        part1 = tab[i+1]
        part2 = (part1.split(end))[0]
        result.append(part2)
    return result


def get_title(main_html_parse):
    balise = main_html_parse.find("title")
    string = str(balise)
    title = find_first_between(string, "<title>", "</title>")
    return title


if __name__ == '__main__':
    user_id = str(sys.argv[1])
    website = talk_to_database.get_website(user_id)

    page = rq.urlopen(website)
    main_html = page.read()
    main_html_parse = BeautifulSoup(main_html, "html.parser")

    registration = Registration(get_title(main_html_parse))
    talk_to_database.insert_compname(user_id, registration.title)

    table_line = main_html_parse.find("table")
    main_info_line = table_line.find_all("div")     # array of divs
    print(main_info_line)
    number_of_events = str(table_line).count("false")-1     #get the number of events
    print(number_of_events)
    all_events = find_all_between(str(table_line), "false);\">", "</div>", number_of_events)    #get all events
    print(all_events)
    for event in all_events:
        talk_to_database.insert_events(user_id, event)      #add event type to database
    for i in range(0,len(main_info_line)):
        if 'class="event"' in str(main_info_line[i]):
            current_event = find_first_between(str(main_info_line[i]), "\">", "</div>")
            print(current_event)
            if current_event in events_list:
                rounds = str(main_info_line[i+1]).count("round")
                print(rounds)
                link = find_first_between(str(main_info_line[i+1]), "window.location=\"", "\"'>")
                link = link.replace("&amp;", "&")
                url = "http://cubecomps.com/"
                url = "{}{}".format(url, link)
                sub_page = rq.urlopen(url)
                sub_html = sub_page.read()
                sub_html_parse = BeautifulSoup(sub_html, "html.parser")
                sub_th_line = sub_html_parse.find_all("th")     # get the format
                try_number = "1"
                form = ""
                for line in sub_th_line:
                    if "t2" in str(line):
                        try_number = "2"
                    if "t3" in str(line):
                        try_number = "3"

                    if "average" in str(line):
                        form = "average of 5"
                        print("average of 5")
                        try_number = "1"
                        break
                    elif "mean" in str(line):
                        form = "mean of 3"
                        print("mean of 3")
                        try_number = "1"
                        break
                    elif "best" in str(line):
                        form = "{}{}".format("best of ", try_number)
                        print("{}{}".format("best of ", try_number))
                        try_number = "1"
                        break

                sub_table_line = sub_html_parse.find_all("table")
                cutoff = "0"
                if "cutoff" in str(sub_table_line[0]):
                    cutoff = find_first_between(str(sub_table_line[0]), "- cutoff ", "</div>")
                    print(cutoff)

                number_of_competitors_in_event = str(sub_table_line[3]).count("compid=")
                print(number_of_competitors_in_event)
                talk_to_database.insert_event_info(user_id, current_event, rounds, form, number_of_competitors_in_event, cutoff)
                competitors_in_event = find_all_between(str(sub_table_line[3]), "compid", "</a>", number_of_competitors_in_event)
                country_of_competitors = find_all_between(str(sub_table_line[3]), "class=\"col_ct\">", "</div>", number_of_competitors_in_event)
                print(country_of_competitors)
                for j in range(0,len(competitors_in_event)):
                    competitors_in_event[j] = competitors_in_event[j].split(">")[1]
                    test = True
                    for current in registration.competitors:
                        comp = Competitors(current, country_of_competitors[j])
                        if comp == competitors_in_event[j]:
                            test = False
                    if test == True:
                        competitor = Competitors(competitors_in_event[j], country_of_competitors[j])
                        registration.competitors.append(competitor)
                        talk_to_database.insert_competitor(user_id, competitor.name, competitor.country)    #add all competitors to the database
                    else:
                        test = True
                print(competitors_in_event)
                for j in range(0, len(competitors_in_event)):
                    talk_to_database.insert_registration(user_id, current_event, competitors_in_event[j])

            print("")
            if current_event == "Competitors":
                number_of_competitors = str(main_info_line[i+1]).count("compid")
                competitors = find_all_between(str(main_info_line[i+1]), "\"'>", "</div>", number_of_competitors)
                print(number_of_competitors)
                print(competitors)
                break

    print(len(registration.competitors))
    for comp in registration.competitors:
        comp.print()



