<?php
 
//Upload a blank cookie.txt to the same directory as this file with a CHMOD/Permission to 777

function get_page($url, $data, $url_dest){
	$fp = fopen("cookie.txt", "w");
    fclose($fp);
    $login = curl_init();
    curl_setopt($login, CURLOPT_COOKIEJAR, "cookie.txt");
    curl_setopt($login, CURLOPT_COOKIEFILE, "cookie.txt");
    curl_setopt($login, CURLOPT_TIMEOUT, 40000);
    curl_setopt($login, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($login, CURLOPT_URL, $url);
    curl_setopt($login, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($login, CURLOPT_REFERER, "http://test.cubecomps.com/admin.php");
    curl_setopt($login, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($login, CURLOPT_POST, TRUE);
    curl_setopt($login, CURLOPT_POSTFIELDS, $data);
    ob_start();
    curl_exec ($login);
    ob_end_clean();
    curl_close ($login);
    unset($login);  

    $sh = curl_init(); 
    curl_setopt($sh, CURLOPT_RETURNTRANSFER,1); 
    curl_setopt($sh, CURLOPT_COOKIEFILE, "cookie.txt"); 
    curl_setopt($sh, CURLOPT_URL, $url_dest); 
    curl_setopt($sh, CURLOPT_REFERER, "http://test.cubecomps.com/admin.php");
    return curl_exec($sh) ;
    curl_close($sh);
}

function login($url,$data){
    $fp = fopen("cookie.txt", "w");
    fclose($fp);
    $login = curl_init();
    curl_setopt($login, CURLOPT_COOKIEJAR, "cookie.txt");
    curl_setopt($login, CURLOPT_COOKIEFILE, "cookie.txt");
    curl_setopt($login, CURLOPT_TIMEOUT, 40000);
    curl_setopt($login, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($login, CURLOPT_URL, $url);
    curl_setopt($login, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($login, CURLOPT_REFERER, "http://test.cubecomps.com/admin.php");
    //curl_setopt($curl, CURLOPT_AUTOREFERER, true); 
    curl_setopt($login, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($login, CURLOPT_POST, TRUE);
    curl_setopt($login, CURLOPT_POSTFIELDS, $data);
    ob_start();
    curl_exec ($login);
    ob_end_clean();
    curl_close ($login);
    unset($login);  

    $sh = curl_init(); 
    curl_setopt($sh, CURLOPT_RETURNTRANSFER,1); 
    curl_setopt($sh, CURLOPT_COOKIEFILE, "cookie.txt"); 
    curl_setopt($sh, CURLOPT_URL,"http://test.cubecomps.com/results.php?cat_id=20"); 
    curl_setopt($sh, CURLOPT_REFERER, "http://test.cubecomps.com/admin.php");
    curl_exec($sh) ;
    curl_close($sh);

    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
    curl_setopt($ch, CURLOPT_COOKIEFILE, "cookie.txt"); 
    curl_setopt($ch, CURLOPT_URL,"test.cubecomps.com/settimes.php?comp_id=1&cat_id=1&round=1&t1=000:00.01&t2=000:00.01&t3=000:00.01&t4=000:00.01&t5=000:00.01 HTTP/1.1"); 
    curl_setopt($ch, CURLOPT_REFERER, "http://test.cubecomps.com/admin.php");
    return curl_exec($ch) ;
    curl_close($ch);

}                
  
 
function grab_page($site){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
    curl_setopt($ch, CURLOPT_COOKIEFILE, "cookie.txt");
    curl_setopt($ch, CURLOPT_URL, $site);
    ob_start();
    return curl_exec ($ch);
    ob_end_clean();
    curl_close ($ch);
}

function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function get_left_string($string, $end){
	$pos = strpos($string, $end);
    if ($pos == 0) return '';
    return substr($string, 0, $pos);
}

function get_right_string($string, $start){
	$pos = strpos($string, $start);
    if ($pos == 0) return '';
    return substr($string, $pos+strlen($start));
}

function get_events($url,$data){
    $output = get_page($url, $data, "http://test.cubecomps.com/events.php");

    $title = get_string_between($output, "<TITLE>", "</TITLE>");
    echo $title;

    $events_table = get_string_between($output, "class=header>", "Add event");
    $events_count = mb_substr_count($events_table, "class=header");

    for($i=0;$i<$events_count;$i++){
    	$current_table = get_left_string($events_table, "class=header>");

    	if (strpos($current_table, "no cutoff") == false) {
    		$time_limit = get_string_between($current_table, "cutoff ", " </span>");
    	}
    	else{
    		$time_limit = "0";
    	}

    	$current_event = get_left_string($current_table, "</div>");
    	$rounds_count = mb_substr_count($current_table, "<b>");
    	for($j=0;$j<$rounds_count;$j++){
    		if($j==0){
    			$current_format = get_string_between($current_table, "cutoff", "people");
    			$current_format = get_string_between($current_format, "'> ", " </span>");
    			
    			$current_people = get_string_between($current_table, $current_format, "people");
    			$current_people = get_string_between($current_people, "- ", " ");	
    		}
    		else{
    			$current_table = get_right_string($current_table, $current_format);
    			$current_format = get_string_between($current_table, $current_people . " people", "people");
    			$current_format = get_string_between($current_format, "'> ", " </span>");

    			$current_people = get_string_between($current_table, $current_format, "people");
    			$current_people = get_string_between($current_people, "- ", " ");
    		}
    		echo $current_format;
    		echo " / ";
    	}
    	echo "<br/>";

       	$events_table = get_right_string($events_table, "class=header>");
    }

}   

function get_competitors($url, $data){
	$output = get_page($url, $data, "http://test.cubecomps.com/competitors.php");

	$comp_table = get_string_between($output, ">m/f<", "id=WCAid");
	$comp_table = get_right_string($comp_table, "</tr>");
	$comp_count = mb_substr_count($comp_table, "comprow");
	for($i=0;$i<$comp_count;$i++){
		$current_comp = get_string_between($comp_table, "<tr", '</tr>');
		
		$comp_id = get_string_between($current_comp, "class=col_cl>", "</div>");

		$current_comp = get_right_string($current_comp, $comp_id);
		$comp_WCAid = get_string_between($current_comp, "class=col_wi>", "</div>");

		$current_comp = get_right_string($current_comp, $comp_WCAid);
		$comp_name = get_string_between($current_comp, "class=col_nm>", "</div>");
		$comp_name = get_string_between($comp_name, "'>", "</a>");

		$current_comp = get_right_string($current_comp, $comp_name);
		$comp_birthday = get_string_between($current_comp, "class=col_bd>", "</div>");

		$current_comp = get_right_string($current_comp, $comp_birthday);
		$comp_country = get_string_between($current_comp, "class=col_ct>", "</div>");

		$current_comp = get_right_string($current_comp, $comp_country);
		$comp_gender = get_string_between($current_comp, "class=col_gd", "</div>");
		$comp_gender = get_right_string($comp_gender, ">");

		$comp_table = get_right_string($comp_table, "</tr>");
	}
}             
 
?>

<?php
    //$output = login("http://test.cubecomps.com/identification.php", "id=94&pw=zrMIHJ7PiB");
    /*$str = get_string_between($output, "var sList = ", ";");
    echo $str;*/
    $url_identification = "http://test.cubecomps.com/identification.php";
    $url_data = "id=94&pw=U9gFF8n9elOE0G1";
    get_events($url_identification, $url_data);
    //get_competitors($url_identification, $url_data);
?>